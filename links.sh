#!/bin/bash
# find links to files in this directory
pwd=$( pwd )
for i in $( ls ); do
  find -L /etc -samefile $pwd/$i 2> /dev/null | tr '\n' ' '
  find -L /var -samefile $pwd/$i 2> /dev/null | tr '\n' ' '
  echo '-->' $i
  echo 
done
